# Dataspace Management Repository

## Git conventions

- Protected main branches
- Branch names derived from issues (`<issue-number>-issue-description`)

## Issue/Merge request conventions

- Based on default templates (`bug` & `feature`)
- Assign at least one type label:
   - Bug
   - Feature
   - Research (sub-type of Feature)
- Assign if relevant a priority label (iff lower or higher than default) 
- Assign if relevant project(s) labels
- Assign if a strict deadline is required a due date

- Merge request must link to an issue
- Merge request must be approved (preferably not by the author)
- All CI pipelines must pass
- The merge request commits must include a change of the `CHANGELOG.md` file 

## Way of working

### Issues
- Maximum of 1 issue labelled `Doing` per repository
- All issues in an iteration should be assigned
- Issues should be split up into smaller issues when the estimated effort is larger than ±10 hours


### Iterations/Milestones/Epics

- Distinction Iteration/Milestone/Epic:
    - Iteration: planning cycle with preferably a given scope
    - Milestone: specific set of features that result in new release(s) of components
    - Epic: Overarching set of feature that support certain use cases
- Issues can be promoted to Milestones, and Milestones to Epics
    - To be discussed when the promotion should happen
    - When an issue is split up this can be the trigger to create a milestone
- New issues can be assigned to Milestones and/or Epics
- Assignment of issues to Iterations happens at the end of each iteration

- 2-week iteration cycle


### Boards
- Three boards envisioned:
   - Development: open, doing, and closed issues
   - Planning: next two iterations planning
   - Capacity: assigned issues per person
- To be discussed further after using for a while.

### Status meetings
- Backlog additions
- Prioritizing issues 
- Divide tasks
- Plan iterations
